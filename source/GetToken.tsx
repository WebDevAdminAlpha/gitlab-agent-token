import React, {FC, useEffect, useState} from 'react';
import {Text, Box, Newline} from 'ink';
import Spinner from 'ink-spinner';
import { ApolloError } from '@apollo/client';
import { registerAgentAndGetToken } from './graphql';

const GetToken: FC<{gitlabUrl:string, projectPath:string, agentName: string, accessToken: string}> = ({gitlabUrl, projectPath, agentName, accessToken}) => {
	const [token, setToken] = useState('')
	const [error, setError] = useState('')
	
	useEffect(() => {
		registerAgentAndGetToken(gitlabUrl, accessToken, projectPath, agentName)
		.then(setToken)
		.catch((error: ApolloError | Error) => {
            if(process.env.DEBUG) {
                console.error(error)
                if(error instanceof ApolloError) {
                    console.log(JSON.stringify(error.graphQLErrors))
                }
            }
            setError(error.message)
        })
	}, [])

	if(error) {
		return (
			<Box borderStyle="single" borderColor="red" paddingX={1}>
			<Text>
				<Text color="red">There was an error:</Text> {error}
			</Text>
			</Box>
		)
	}

	if(!token) {
		return (
			<Text>
				<Text color="green">
					<Spinner type="dots" />
				</Text>
				{' Loading'}
			</Text>
		)
	}

	return (
		<Box flexDirection="column" borderStyle="single" borderColor="blue">
			<Text>The agent's authorization token is:</Text>
			<Newline></Newline>
			<Text color="blue">{token}</Text>
			<Newline></Newline>
			<Text>Apply this secret in your cluster (the namespace can be changed):</Text>
			<Newline></Newline>
			<Text color="blue">kubectl create secret generic -n agent-namespace gitlab-agent-token --from-literal=token='{token}'</Text>
			<Newline></Newline>
			<Text>Now, install the agent into your cluster following our documentation: https://bit.ly/3mACJ6p</Text>
		</Box>
	)
}

export default GetToken