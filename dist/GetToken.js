"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const ink_1 = require("ink");
const ink_spinner_1 = __importDefault(require("ink-spinner"));
const client_1 = require("@apollo/client");
const graphql_1 = require("./graphql");
const GetToken = ({ gitlabUrl, projectPath, agentName, accessToken }) => {
    const [token, setToken] = react_1.useState('');
    const [error, setError] = react_1.useState('');
    react_1.useEffect(() => {
        graphql_1.registerAgentAndGetToken(gitlabUrl, accessToken, projectPath, agentName)
            .then(setToken)
            .catch((error) => {
            if (process.env.DEBUG) {
                console.error(error);
                if (error instanceof client_1.ApolloError) {
                    console.log(JSON.stringify(error.graphQLErrors));
                }
            }
            setError(error.message);
        });
    }, []);
    if (error) {
        return (react_1.default.createElement(ink_1.Box, { borderStyle: "single", borderColor: "red" },
            react_1.default.createElement(ink_1.Text, null,
                react_1.default.createElement(ink_1.Text, { color: "red" }, "There was an error:"),
                " ",
                error)));
    }
    if (!token) {
        return (react_1.default.createElement(ink_1.Text, null,
            react_1.default.createElement(ink_1.Text, { color: "green" },
                react_1.default.createElement(ink_spinner_1.default, { type: "dots" })),
            ' Loading'));
    }
    return (react_1.default.createElement(ink_1.Box, { flexDirection: "column", borderStyle: "single", borderColor: "blue" },
        react_1.default.createElement(ink_1.Text, null, "The agent's authorization token is:"),
        react_1.default.createElement(ink_1.Newline, null),
        react_1.default.createElement(ink_1.Text, { color: "blue" }, token),
        react_1.default.createElement(ink_1.Newline, null),
        react_1.default.createElement(ink_1.Text, null, "Apply this secret in your cluster (the namespace can be changed):"),
        react_1.default.createElement(ink_1.Newline, null),
        react_1.default.createElement(ink_1.Text, { color: "blue" },
            "kubectl create secret generic -n agent-namespace gitlab-agent-token --from-literal=token='",
            token,
            "'"),
        react_1.default.createElement(ink_1.Newline, null),
        react_1.default.createElement(ink_1.Text, null, "Now, install the agent into your cluster following our documentation: https://bit.ly/3mACJ6p")));
};
exports.default = GetToken;
