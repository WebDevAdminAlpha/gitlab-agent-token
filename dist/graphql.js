"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerAgentAndGetToken = void 0;
const client_1 = require("@apollo/client");
const fetch = require('node-fetch');
async function checkIfAgentExists(client, projectPath, agentName) {
    var _a, _b;
    let query = client_1.gql `query {
		project (
		  fullPath: "${projectPath}"
		) {
			fullPath
			clusterAgents {
			  nodes {
				name
				id
			  }
			}
		}
	  }`;
    const resp = await client.query({ query });
    if (!((_a = resp.data) === null || _a === void 0 ? void 0 : _a.project)) {
        throw new Error(`Project \`${projectPath}\` does not exist. Create it first!`);
    }
    if ((_b = resp.data) === null || _b === void 0 ? void 0 : _b.project.clusterAgents.nodes.filter((a) => a.name == agentName)) {
        return resp.data.project.clusterAgents.id;
    }
    else {
        return null;
    }
}
async function createClusterAgent(client, projectPath, agentName) {
    var _a, _b, _c, _d;
    let mutation = client_1.gql `
	mutation createAgent {
		createClusterAgent(input: { projectPath: "${projectPath}", name: "${agentName}" }) {
		  clusterAgent {
			id
			name
		  }
		  errors
		}
	}`;
    const resp = await client.mutate({ mutation, variables: null });
    if ((_c = (_b = (_a = resp.data) === null || _a === void 0 ? void 0 : _a.createClusterAgent) === null || _b === void 0 ? void 0 : _b.clusterAgent) === null || _c === void 0 ? void 0 : _c.id) {
        return (_d = resp.data) === null || _d === void 0 ? void 0 : _d.createClusterAgent.clusterAgent.id;
    }
    throw new Error(resp.data.createClusterAgent.errors[0]);
}
async function createToken(client, agentId) {
    var _a, _b;
    let mutation = client_1.gql `
	mutation createToken {
		clusterAgentTokenCreate(input: { clusterAgentId: "${agentId}" }) {
		  secret
		  token {
			createdAt
			id
		  }
		  errors
		}
	}`;
    const resp = await client.mutate({ mutation, variables: null });
    if ((_b = (_a = resp.data) === null || _a === void 0 ? void 0 : _a.clusterAgentTokenCreate) === null || _b === void 0 ? void 0 : _b.secret) {
        return resp.data.clusterAgentTokenCreate.secret;
    }
    throw new Error(resp.data.clusterAgentTokenCreate.errors[0]);
}
async function registerAgentAndGetToken(gitlabUrl, accessToken, projectPath, agentName) {
    const client = new client_1.ApolloClient({
        uri: `${gitlabUrl}`,
        link: new client_1.HttpLink({
            uri: `${gitlabUrl}/api/graphql`,
            headers: {
                'authorization': `Bearer ${accessToken}`
            },
            fetch
        }),
        cache: new client_1.InMemoryCache()
    });
    let agentId = await checkIfAgentExists(client, projectPath, agentName);
    if (!agentId) {
        agentId = await createClusterAgent(client, projectPath, agentName);
    }
    return createToken(client, agentId);
}
exports.registerAgentAndGetToken = registerAgentAndGetToken;
