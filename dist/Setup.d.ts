import { FC } from 'react';
import { StringSchema } from 'joi';
declare const Setup: FC<{
    title: string;
    placeholder: string;
    defaultValue: string;
    helpText?: string;
    onSubmit: (value: string) => void;
    hidden?: boolean;
    schema: StringSchema;
}>;
export default Setup;
