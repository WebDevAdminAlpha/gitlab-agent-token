# GitLab Agent Token generator

This script helps setting up a new GitLab Kubernetes Agent by providing a CLI script to register a new agent with a GitLab instance and retrieve the agent's token.

The GitLab Kubernetes Agent is GitLab's cluster side component to support GitLab's Kubernetes integration features like pull based deployments. Once the token is available, it [should be added to your cluster as a secret](https://docs.gitlab.com/ee/user/clusters/agent/#create-the-kubernetes-secret). See [the GitLab documentation](https://docs.gitlab.com/ee/user/clusters/agent/) for more details about setting up the GitLab Kubernetes Agent.

## Usage

```bash
$ npx @gitlab/gitlab-agent-token
```

## Todo

See the issues: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent-token/-/issues

## Release

Release a new version with `npm version ...` run locally
